package hr.dturic.personal.tdkappdemoadler.cards

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import hr.dturic.personal.tdkappdemoadler.R
import hr.dturic.personal.tdkappdemoadler.common.Status.*
import hr.dturic.personal.tdkappdemoadler.common.base.BaseFragment
import hr.dturic.personal.tdkappdemoadler.log.Alder
import hr.dturic.personal.tdkappdemoadler.model.Card
import kotlinx.android.synthetic.main.fragment_cards.*
import javax.inject.Inject

@AndroidEntryPoint
class CardsFragment : BaseFragment(R.layout.fragment_cards) {

    companion object {
        const val TAG = "CardsFragment"
    }

    private val viewModel: CardsViewModel by activityViewModels()

    @Inject
    lateinit var cardsAdapter: CardsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        bind()
    }

    private fun setupUI() {
        with(cardsList) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = cardsAdapter
        }

        appBar.infoButtonListener = { showDebugSnackBar() }

        cardsAdapter.itemClickListener = {
            Alder.log(TAG, "Open card: ${it.cardId}")

            // TODO Present CardDetailsFragment Hint -> Navigation
        }
    }

    private fun bind() {
        viewModel.cardsLiveData.observe(requireActivity()) {
            when (it.status) {
                LOADING -> onLoading(true)
                SUCCESS -> onSuccess(it.data!!)
                ERROR -> onError(it.message ?: "")
            }
        }
    }

    private fun onSuccess(cards: List<Card>) {
        super.onSuccess()
        cardsAdapter.cards = cards
    }
}