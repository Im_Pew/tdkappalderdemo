package hr.dturic.personal.tdkappdemoadler.log

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings

class Builder(private val context: Context) {

    private var regexList = emptyList<String>()

    var secretKet: String = ""

    fun setRegexList(list: List<String>): Builder {
        regexList = list
        return this
    }

    fun setAppSecretKey(key: String): Builder {
        secretKet = key
        return this
    }

    @SuppressLint("HardwareIds")
    fun retrieveDeviceId(context: Context): String = Settings.Secure.getString(
        context.contentResolver, Settings.Secure.ANDROID_ID
    )

    fun build() {
        val alderConfig = AlderConfig

        alderConfig.context = this.context
        alderConfig.regexList = this.regexList
        alderConfig.appSecretKey = this.secretKet
        alderConfig.deviceId = this.retrieveDeviceId(context)
        alderConfig.url = AlderPreferences.getLink(context)
    }
}