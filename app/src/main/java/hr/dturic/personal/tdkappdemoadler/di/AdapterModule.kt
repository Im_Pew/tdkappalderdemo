package hr.dturic.personal.tdkappdemoadler.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hr.dturic.personal.tdkappdemoadler.cards.CardsAdapter

@Module
@InstallIn(SingletonComponent::class)
class AdapterModule {

    @Provides
    fun provideCardAdapter() = CardsAdapter()
}
