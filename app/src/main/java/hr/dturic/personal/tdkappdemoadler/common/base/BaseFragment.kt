package hr.dturic.personal.tdkappdemoadler.common.base

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import hr.dturic.personal.tdkappdemoadler.common.LoadingFragment
import hr.dturic.personal.tdkappdemoadler.log.AlderConfig
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

open class BaseFragment(@LayoutRes layout: Int) : Fragment(layout), StateListener {

    private val mainScope = CoroutineScope(Dispatchers.Main)

    private var loadingFragment: LoadingFragment? = null

    override fun onResume() {
        super.onResume()

        loadingFragment = LoadingFragment()
    }

    override fun onSuccess() {
        onLoading(false)
    }

    override fun onLoading(show: Boolean) {
        lifecycleScope.launchWhenCreated {
            if (show) loadingFragment?.show(
                requireActivity().supportFragmentManager,
                "loading_fragment"
            )
            else if (loadingFragment?.isVisible == true) loadingFragment?.dismiss()
        }
    }

    override fun onError(error: String) {
        onLoading(false)
        mainScope.launch {
            Snackbar.make(requireView(), error, Snackbar.LENGTH_LONG).show()
        }
    }

    protected fun showDebugSnackBar() {
        val code = AlderConfig.deviceId
        val snackBar = Snackbar.make(requireView(), code, Snackbar.LENGTH_INDEFINITE)

        snackBar.setAction(
            "Close"
        ) {
            snackBar.dismiss()
        }.show()
    }
}