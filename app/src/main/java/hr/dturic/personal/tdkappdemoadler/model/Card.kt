package hr.dturic.personal.tdkappdemoadler.model

import com.squareup.moshi.Json

data class Card(
    @field:Json(name = "card_id") val cardId: String,
    @field:Json(name = "card_name") val name: String,
    @field:Json(name = "card_expiration_date") val expirationDate: String,
    @field:Json(name = "card_logo") val logo: String,
    @field:Json(name = "card_code") val cardCode: String
)