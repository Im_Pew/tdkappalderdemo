package hr.dturic.personal.tdkappdemoadler.cards

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import hr.dturic.personal.tdkappdemoadler.R
import hr.dturic.personal.tdkappdemoadler.Utils
import hr.dturic.personal.tdkappdemoadler.Utils.loadImageInto
import hr.dturic.personal.tdkappdemoadler.log.Alder
import hr.dturic.personal.tdkappdemoadler.model.Card
import kotlinx.android.extensions.LayoutContainer

class CardsAdapter : RecyclerView.Adapter<CardsAdapter.CardViewHolder>() {

    companion object {
        private val TAG = "CardsAdapter"
    }

    var cards: List<Card> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var itemClickListener: (Card) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_card, parent, false)
        return CardViewHolder(view)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        holder.onBind(cards[position], itemClickListener)
    }

    override fun getItemCount() = cards.size

    inner class CardViewHolder(view: View) : RecyclerView.ViewHolder(view), LayoutContainer {
        override val containerView = view

        fun onBind(card: Card, listener: (Card) -> Unit) {
            val context = itemView.context

            val logoImage = itemView.findViewById<AppCompatImageView>(R.id.cardIcon)
            logoImage.loadImageInto(card.logo)

            itemView.findViewById<TextView>(R.id.cardName).text = card.name
            itemView.findViewById<TextView>(R.id.cardCode).text =
                Utils.censureCardCode(card.cardCode)
            itemView.findViewById<TextView>(R.id.cardExpirationDate).text =
                context.getText(R.string.censured_expiration_date)

            when (Utils.checkIfCardIsExpired(card.expirationDate)) {
                true -> {
                    Alder.log(
                        TAG,
                        "Hidden expired card: ${Utils.censureCardCode(card.cardCode)}(${card.expirationDate})"
                    )
                    itemView.setBackgroundColor(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.semi_transparent_color
                        )
                    )
                }
                false -> {
                    itemView.setOnClickListener { listener(card) }
                }
            }
        }
    }
}