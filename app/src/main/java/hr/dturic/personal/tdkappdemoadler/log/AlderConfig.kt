package hr.dturic.personal.tdkappdemoadler.log

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import javax.net.ssl.HttpsURLConnection

internal object AlderConfig {

    private const val AUTHORISATION_URL = "https://alder.shape404.agency/api/sas/"

    internal var regexList: List<String> = emptyList()

    internal var url: String = ""

    internal var deviceId: String = ""

    internal var context: Context? = null

    internal var appSecretKey: String = ""

    internal suspend fun retrieveUrl() {
        if (url.isNotEmpty()) return

        withContext(MainScope().coroutineContext + Dispatchers.Default) {
            with(URL(AUTHORISATION_URL).openConnection() as HttpsURLConnection) {
                requestMethod = "POST"
                addRequestProperty("Authorization", appSecretKey)
                addRequestProperty("Content-Type", "application/json")

                val requestData = JSONObject().apply {
                    put("deviceId", deviceId)
                }

                with(outputStream) {
                    write(requestData.toString().toByteArray())
                    flush()
                }

                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    val link = response.toString()

                    AlderPreferences.saveLink(context!!, link)
                    AlderConfig.url = link
                }
            }
        }
    }
}