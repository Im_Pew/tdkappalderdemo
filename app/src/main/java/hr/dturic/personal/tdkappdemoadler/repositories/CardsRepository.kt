package hr.dturic.personal.tdkappdemoadler.repositories

import hr.dturic.personal.tdkappdemoadler.api.CardsApi
import hr.dturic.personal.tdkappdemoadler.common.State
import hr.dturic.personal.tdkappdemoadler.common.ResponseHandler
import hr.dturic.personal.tdkappdemoadler.model.Card
import hr.dturic.personal.tdkappdemoadler.model.CardDetails
import javax.inject.Inject

class CardsRepository @Inject constructor(
    private val cardsApi: CardsApi
) {

    suspend fun fetchCards(): State<List<Card>> {
        return try {
            ResponseHandler.handleSuccess(cardsApi.getCards())
        } catch (e: Exception) {
            ResponseHandler.handleException(e)
        }
    }

    suspend fun fetchCard(id: String): State<CardDetails> {
        return try {
            ResponseHandler.handleSuccess(cardsApi.getCard(id))
        } catch (e: Exception) {
            ResponseHandler.handleException(e)
        }
    }
}