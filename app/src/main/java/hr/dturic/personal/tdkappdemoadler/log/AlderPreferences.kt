package hr.dturic.personal.tdkappdemoadler.log

import android.content.Context
import android.content.Context.MODE_PRIVATE

object AlderPreferences {

    private const val LINKS_PREFERENCES = "hr.dturic.personal.tdkappdemoadler.shared_preferences"

    private const val SEND_LOG_LINK = "hr.dturic.personal.tdkappdemoadler.LINK"

    fun saveLink(context: Context, url: String) {
        val sharedPreferences = getPreferences(context)

        sharedPreferences.edit().putString(SEND_LOG_LINK, url).apply()
    }

    fun getLink(context: Context): String {
        val preferences = getPreferences(context)
        return preferences.getString(SEND_LOG_LINK, "") ?: ""
    }

    private fun getPreferences(context: Context) =
        context.getSharedPreferences(LINKS_PREFERENCES, MODE_PRIVATE)
}