package hr.dturic.personal.tdkappdemoadler.cards

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import hr.dturic.personal.tdkappdemoadler.common.State
import hr.dturic.personal.tdkappdemoadler.model.Card
import hr.dturic.personal.tdkappdemoadler.model.CardDetails
import hr.dturic.personal.tdkappdemoadler.repositories.CardsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CardsViewModel @Inject constructor(
    private val cardsRepository: CardsRepository,
    private val coroutineScope: CoroutineScope,
) : ViewModel() {

    val cardsLiveData = MutableLiveData<State<List<Card>>>()
    val cardDetailsLiveData = MutableLiveData<State<CardDetails>>()

    init {
        fetchData()
    }

    fun fetchData() = coroutineScope.launch {
        cardsLiveData.postValue(State.loading(null))
        val cards = cardsRepository.fetchCards()
        cardsLiveData.postValue(cards)
    }

    fun fetchCardData(cardId: String) = coroutineScope.launch {
        cardDetailsLiveData.postValue(State.loading(null))
        val card = cardsRepository.fetchCard(cardId)
        cardDetailsLiveData.postValue(card)
    }
}