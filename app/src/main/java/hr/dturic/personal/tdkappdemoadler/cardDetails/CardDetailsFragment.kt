package hr.dturic.personal.tdkappdemoadler.cardDetails

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import hr.dturic.personal.tdkappdemoadler.R
import hr.dturic.personal.tdkappdemoadler.cards.CardsViewModel
import hr.dturic.personal.tdkappdemoadler.common.Navigation
import hr.dturic.personal.tdkappdemoadler.common.Status.*
import hr.dturic.personal.tdkappdemoadler.common.base.BaseFragment
import hr.dturic.personal.tdkappdemoadler.model.CardDetails
import kotlinx.android.synthetic.main.fragment_card_details.*

class CardDetailsFragment(private val cardId: String) :
    BaseFragment(R.layout.fragment_card_details) {

    private val viewModel: CardsViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        bind()
    }

    override fun onStart() {
        super.onStart()
        viewModel.fetchCardData(cardId)
    }

    private fun setupUI() {
        cardSettingButtons.setOnClickListener {
            Toast.makeText(
                requireContext(),
                getString(R.string.label_not_available),
                Toast.LENGTH_SHORT
            ).show()
        }

        appBar.infoButtonListener = { showDebugSnackBar() }
        appBar.onBackButton = { Navigation.popup() }
    }

    private fun bind() {
        viewModel.cardDetailsLiveData.observe(requireActivity()) {
            when (it.status) {
                SUCCESS -> onSuccess(it.data!!)
                ERROR -> onError(it.message ?: "")
                LOADING -> Unit
            }
        }
    }

    private fun onSuccess(cardDetails: CardDetails) {
        super.onSuccess()

        lifecycleScope.launchWhenCreated {
            // TODO Present data to user
            // Hints:
            // cardLogo, cardCodeValue, sendReceiveStatus, sendReceiveValue
            // appBar.title
            // Utils.censureCardCode, Utils.prepareSendReceiveStatus
        }
    }
}