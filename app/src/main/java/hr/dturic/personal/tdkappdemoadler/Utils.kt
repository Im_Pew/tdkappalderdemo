package hr.dturic.personal.tdkappdemoadler

import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import hr.dturic.personal.tdkappdemoadler.views.SendReceiveView
import java.text.SimpleDateFormat
import java.util.*

object Utils {
    fun AppCompatImageView.loadImageInto(imageUrl: String) =
        Glide.with(this.context)
            .load(imageUrl)
            .placeholder(R.drawable.icaccountbankaplaceholder)
            .into(this)

    fun censureCardCode(code: String) =
        code.replaceRange(5, 15, "**** ****")

    fun checkIfCardIsExpired(expirationDate: String): Boolean {
        val formatChecker = Regex("^\\d\\d/\\d\\d\$")

        if (!formatChecker.matches(expirationDate)) return false

        val values = expirationDate.split("/")
        val parsedCurrentDate =
            SimpleDateFormat("MM/yy", Locale.getDefault())
                .format(Date())
                .split("/")

        return values[1].toInt() <= parsedCurrentDate[1].toInt() &&
                values[0].toInt() <= parsedCurrentDate[0].toInt()
    }

    fun prepareSendReceiveStatus(sendReceiveView: SendReceiveView): String {
        return "S karticom možeš: ${sendReceiveView.getStatusAsString()}"
    }
}
