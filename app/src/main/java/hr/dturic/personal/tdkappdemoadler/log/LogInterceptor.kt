package hr.dturic.personal.tdkappdemoadler.log

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody


class LogInterceptor : Interceptor {

    companion object {
        private const val TAG = "*"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        val body = response.body?.string()
        Alder.log(TAG, "\n$body\n")

        return response.newBuilder()
            .body(body?.toByteArray()?.toResponseBody(response.body?.contentType()))
            .build()
    }
}