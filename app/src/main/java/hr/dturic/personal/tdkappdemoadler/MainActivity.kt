package hr.dturic.personal.tdkappdemoadler

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import hr.dturic.personal.tdkappdemoadler.cards.CardsFragment
import hr.dturic.personal.tdkappdemoadler.common.Navigation

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Navigation.replace(CardsFragment())
    }
}