package hr.dturic.personal.tdkappdemoadler.common

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import hr.dturic.personal.tdkappdemoadler.R

class LoadingFragment : DialogFragment(R.layout.fragment_load) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.Theme_App_Dialog_FullScreen)
    }
}