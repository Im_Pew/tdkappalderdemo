package hr.dturic.personal.tdkappdemoadler.common

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

object Navigation {

    private var fragmentManager: FragmentManager? = null
    private var containerId: Int? = null

    private val stack = mutableListOf<Fragment>()

    fun setContainer(@IdRes id: Int) {
        containerId = id
    }

    fun setFragmentManager(fragmentManager: FragmentManager) {
        this.fragmentManager = fragmentManager
    }

    fun replace(fragment: Fragment) {
        setFragment(fragment)
        stack.add(fragment)
    }

    fun popup() {
        if (stack.size == 1) return
        stack.removeLast()
        setFragment(stack.last())
    }

    private fun setFragment(fragment: Fragment) =
        containerId?.let { fragmentManager?.beginTransaction()?.replace(it, fragment)?.commit() }
}