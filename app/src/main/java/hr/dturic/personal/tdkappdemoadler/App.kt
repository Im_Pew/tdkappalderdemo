package hr.dturic.personal.tdkappdemoadler

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.HiltAndroidApp
import hr.dturic.personal.tdkappdemoadler.common.Navigation
import hr.dturic.personal.tdkappdemoadler.log.Builder

@HiltAndroidApp
class App : Application(), Application.ActivityLifecycleCallbacks {

    override fun onCreate() {
        super.onCreate()

        registerActivityLifecycleCallbacks(this)
        Navigation.setContainer(android.R.id.content)

        Builder(applicationContext).setAppSecretKey(BuildConfig.ALDER_SECRET_KEY)
            .setRegexList(listOf())
            .build()
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {
        Navigation.setFragmentManager((p0 as AppCompatActivity).supportFragmentManager)
        addBackButtonListener(p0)
    }

    override fun onActivityStarted(p0: Activity) {
        Navigation.setFragmentManager((p0 as AppCompatActivity).supportFragmentManager)
        addBackButtonListener(p0)
    }

    override fun onActivityResumed(p0: Activity) {
        Navigation.setFragmentManager((p0 as AppCompatActivity).supportFragmentManager)
        addBackButtonListener(p0)
    }

    override fun onActivityPaused(p0: Activity) {

    }

    override fun onActivityStopped(p0: Activity) {

    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {

    }

    override fun onActivityDestroyed(p0: Activity) {

    }

    private fun addBackButtonListener(activity: AppCompatActivity) {
        activity.onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.popup()
            }
        })
    }
}