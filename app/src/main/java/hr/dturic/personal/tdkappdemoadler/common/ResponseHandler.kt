package hr.dturic.personal.tdkappdemoadler.common

import retrofit2.HttpException
import java.net.SocketTimeoutException

object ResponseHandler {
    fun <T : Any> handleSuccess(data: T): State<T> {
        return State.success(data)
    }

    fun <T : Any> handleException(e: Exception): State<T> {
        return when (e) {
            is HttpException -> State.error(getErrorMessage(e.code()), null)
            is SocketTimeoutException -> State.error("Request time out", null)
            else -> State.error(getErrorMessage(Int.MAX_VALUE), null)
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            401 -> "Unauthorised"
            404 -> "404: Site not found"
            else -> "Something went wrong"
        }
    }
}