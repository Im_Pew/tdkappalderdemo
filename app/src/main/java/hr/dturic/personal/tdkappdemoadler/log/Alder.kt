package hr.dturic.personal.tdkappdemoadler.log

import android.annotation.SuppressLint
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import javax.net.ssl.HttpsURLConnection

object Alder {
    private var config: AlderConfig = AlderConfig

    private val scope = CoroutineScope(Dispatchers.Default + CoroutineName("Alder"))

    private val END_LINE =
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            System.lineSeparator().toByteArray()
        } else {
            (System.getProperty("line.separator") ?: "\n").toByteArray()
        }

    @SuppressLint("HardwareIds")
    fun log(tag: String, log: String) {
        scope.launch(Dispatchers.Default) {
            AlderConfig.retrieveUrl()
            sendLog("$tag: $log")
        }
    }

    private suspend fun sendLog(log: String) {
        with(URL(config.url).openConnection() as HttpsURLConnection) {
            requestMethod = "PUT"

            with(outputStream) {
                write(log.toByteArray())
                write(END_LINE)
                flush()
            }

            BufferedReader(InputStreamReader(inputStream)).use {
                val response = StringBuffer()

                var inputLine = it.readLine()
                while (inputLine != null) {
                    response.append(inputLine)
                    inputLine = it.readLine()
                }
                println("Response : $response")
            }
        }
    }

    private fun parseWithRegex(string: String): String {
        var parsed = string
        config.regexList.forEach {
            val regex = Regex("\"$it\"\\s?:\\s?\"([\\d\\w\\S\\s])\\s?(.*?)\"")

            if (parsed.contains(regex)) {
                parsed = regex.replace(parsed, "$it\":\"*****\"")
            }
        }

        return parsed
    }
}